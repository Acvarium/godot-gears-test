extends Node2D

var lines = []
var a = Vector2(0,1)
var current_color = Color.red
var colors = []

func _ready():
	pass # Replace with function body.

func clear_lines():
	lines.clear()
	colors.clear()
	current_color = Color.red

func append_line_data(line):
	lines.append(line)
	colors.append(current_color)

func add_line_points(point_a_x, point_a_y, point_b_x, point_b_y):
	append_line_data([Vector2(point_a_x, point_a_y), Vector2(point_b_x, point_b_y)])

func add_line_vectors(point_a, point_b):
	append_line_data([point_a, point_b])

func add_line(line):
	append_line_data(line)

func set_color(col):
	current_color = col

func _draw():
	for i in range(lines.size()):
		var l = lines[i]
		draw_line(l[0], l[1], colors[i], 2)
