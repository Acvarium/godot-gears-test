extends Node2D
export var teeth = 13
export var radius_a = 150
export var radius_b = 130
var mouse_over = false

func _ready():
	draw_gear()

func draw_gear():
	$Canvas.clear_lines()
	var pos = Vector2(0, radius_b)
	var rad_a = radius_a + (radius_a - radius_b) 
	var rad_b = radius_b - (radius_a - radius_b)
	
	pos = pos.normalized() * rad_b
	for i in range(teeth * 2):
		var length = rad_a 
		if i % 2:
			length = rad_b 
		var pos_a = pos
		var pos_b = pos.rotated((PI * 2) / (teeth * 2)).normalized() * length
		var gear_pos_a = pos_a + (pos_b - pos_a) / 3
		var gear_pos_b = pos_a + (pos_b - pos_a) / 3 * 2
#		$Canvas.add_line_vectors(pos_a, pos_b)
		$Canvas.add_line_vectors(pos_a.normalized() * gear_pos_a.length(), gear_pos_a)
		$Canvas.add_line_vectors(gear_pos_a, gear_pos_b)
		$Canvas.add_line_vectors(pos_b.normalized() * gear_pos_b.length(), gear_pos_b)
		pos = pos_b
	

#	for i in range(teeth * 2):
#		var pos_a = pos
#		var pos_b = pos.rotated((PI * 2) / (teeth * 2))
#		$Canvas.set_color(Color.blue)
#		$Canvas.add_line_vectors(pos_a.normalized() * radius_a, pos_b.normalized() * radius_a)
#		$Canvas.set_color(Color.green)
#		$Canvas.add_line_vectors(pos_a.normalized() * radius_b, pos_b.normalized() * radius_b)
#		pos = pos_b

	$Canvas.update()

func _on_Area2D_mouse_entered():
	mouse_over = true


func _on_Area2D_mouse_exited():
	mouse_over = false


func _input(event):
	if mouse_over and event.is_action_pressed("w_up"):
		teeth += 1
		draw_gear()
	elif mouse_over and event.is_action_pressed("w_down"):
		teeth -= 1
		if teeth < 3:
			teeth = 3
		draw_gear()
