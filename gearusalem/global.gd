extends Node
var save_folder = "res://levels/" 

var current_scene = null
enum gear_material {WOOD, RUST, STILL, BRONZ, SILBER, GOLD}
var game_mode = false


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	list_levels()
#	add_loading_screen()


func goto_scene(path):
	call_deferred("_deferred_goto_scene",path)


func _deferred_goto_scene(path):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)


func get_level_file_path(level_name):
	return save_folder + level_name + ".level"
	

func save_level(level_name):
	var save_file = File.new()
	save_file.open(get_level_file_path(level_name), File.WRITE)
	var saved_nodes = get_tree().get_nodes_in_group("save")
	for node in saved_nodes:
		var node_details = node.get_save_stats()
		save_file.store_line(to_json(node_details))
	save_file.close()


func load_saved(level_name):
	if not(level_name in list_levels()):
		return
	LoadingScreen.show_loading_screen()
	var main_node = get_tree().get_root().get_node("main")
	main_node.clean_scene()
	var save_filename = get_level_file_path(level_name)
	var save_file = File.new()
	if not save_file.file_exists(save_filename):
		return
	save_file.open(save_filename, File.READ)
	while save_file.get_position() < save_file.get_len():
		var node_data = parse_json(save_file.get_line())
		
		if "filename" in node_data.keys():
			var item_file = load(node_data.filename)
			var item = item_file.instance()
			get_node(node_data.parent_path).add_child(item)
			item.load_save_stats(node_data)
		elif "node_path" in node_data.keys():
			get_node(node_data.node_path).load_save_stats(node_data)
	main_node.pin_loaded_gears()
	main_node.rebuild_timer_start()
	LoadingScreen.hide_loading_screen()


func list_levels():
	var files = []
	var dir = Directory.new()
	if dir.open(save_folder) == OK:
		dir.list_dir_begin(true)
		var file = dir.get_next()
		while file != '':
			files.append(file.split('.')[0])
			file = dir.get_next()
	else:
		push_error("An error occurred when trying to access the path.")
	return files
