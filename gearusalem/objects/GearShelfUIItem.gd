extends Panel

onready var main_node = get_tree().get_root().get_node("main")
var gear_holds = null

func set_gear(gear):
	gear_holds = gear
	$Label.text = str(gear.teeth)
	

func _on_Button_button_down():
	main_node.remove_gear_from_shelf(gear_holds, self)
