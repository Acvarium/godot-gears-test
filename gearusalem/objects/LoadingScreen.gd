extends CanvasLayer


func show_loading_screen():
	visible = true
	
func hide_loading_screen():
	$Timer.start()

func _on_Timer_timeout():
	visible = false
