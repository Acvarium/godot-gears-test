extends KinematicBody2D
var selected = false
var mouse_ower = false
var in_contact
#var is_in_contact = false
var start_rot = 0.0
export(float) var _speed = 0.0
export(int) var teeth = 14
onready var main_node = get_tree().get_root().get_node("main")
var W = Color(1,1,1,0.5)
var R = Color(1,0.2,0.2)

const default_radius = 135
const default_teeth = 14
var radius = 135
var toothObj = load("res://objects/tooth.tscn")
const TOOTH_SIZE = 22
var rot_offset = PI / 30
var prev_rot = 0
var locked = false
var is_on_shelf = false
var to_pin_gear_to 
var pickup_snap_pos = null

func _ready():
	generate()
	if _speed != 0:
		modulate = R


func get_item_info():
	var item_info_dict = {}
	item_info_dict.type = "gear"
	item_info_dict.name = name
	return item_info_dict


func generate():
#  Видалення всіх зубців перед генерацією
	for t in $teeth.get_children():
		t.queue_free()
	var r = 0.0													#  кур, на який повернуто зубець
	var c_stap = (default_radius * 2 * PI) / default_teeth		#  відстань між зубцями по окружності
	var C = c_stap * teeth										#  довжина кола
	radius = C/(2*PI)											#  радіус шестерні
	
#  підгонка візуальних даних
	$text/teeth.text = str(teeth)
	
	var c_scale = (radius + TOOTH_SIZE) / 161 
	$collision.scale = Vector2(c_scale, c_scale)
	c_scale = (radius + TOOTH_SIZE) / 112
	$Area2D/collision.scale = Vector2(c_scale, c_scale)
	
	c_scale = (radius - TOOTH_SIZE) / 127 
	$circle.scale = Vector2(c_scale, c_scale)
	$text/teeth3.text = str(radius) + ' ' + str(c_scale)

#  послідовна генерація зубців, один за одним
	for i in range(teeth):
		var t = toothObj.instance()
		t.rotation = r												#  орієнтація зубця
		var p = Vector2(sin(-r), cos(-r)).normalized() * (radius - 2)		#  позиція зубця відносно центра шестерні
		t.position = p
		$teeth.add_child(t)
		r -= PI*2 / teeth											#  зміження кута, для генерації наступного зубця


func disconnect_gear(gear):
	if in_contact == null or in_contact.get_ref() == null:
		in_contact = null
		return
	if gear != null and in_contact != null and gear == in_contact.get_ref():
		 contact_gear(null)


func put_on_shelf():
	drop()
	is_on_shelf = true
	visible = false
	
func remove_from_shelf():
	is_on_shelf = false
	visible = true

func contact_gear(gear):
	if gear == null:
		in_contact = null
		for g in get_tree().get_nodes_in_group("gear"):
			g.disconnect_gear(self)
	else:
		if gear.in_contact != null:
			if gear.in_contact.get_ref() == self:
				return
		in_contact = weakref(gear)
		update_rot_offset(gear)
		if main_node.is_gear_pinned(self) == null and !Global.game_mode:
			snap_to_gear(gear)


func is_some_gear_selected():
	var result = false
	for g in get_tree().get_nodes_in_group("gear"):
		if g.selected:
			result = true
			break
	return result


func snap_to_gear(gear):
	var snap_gear_pos = gear.global_position
	var snap_gear_req_dist = gear.get_gear_radius() + get_gear_radius() - TOOTH_SIZE + 5
	var snap_vec = gear.global_position.direction_to(global_position).normalized() * snap_gear_req_dist
	global_position = gear.global_position + snap_vec


func pickup(from_shelf = false):
	pickup_snap_pos = global_position
	if from_shelf:
		pickup_snap_pos = null
	contact_gear(null)
	selected = true
	
	
func drop():
	pickup_snap_pos = null
	main_node.rebuild_gear_struct()
	selected = false


func _input(event):
	if mouse_ower:
		if Input.is_action_just_released("w_up") and !Global.game_mode:
			teeth += 1
			generate()
			main_node.rebuild_gear_struct()
		if Input.is_action_just_released("w_down") and !Global.game_mode:
			teeth -= 1
			if teeth < 4:
				teeth = 4
			generate()
			main_node.rebuild_gear_struct()


func get_gear_radius():
	return radius + TOOTH_SIZE / 3
	

func find_all_connections(to_connect = true):
	var gears_in_range = []
	for g in get_tree().get_nodes_in_group("gear"):
		if Global.game_mode and !main_node.is_gear_pinned(g):
			continue
		if g.is_on_shelf:
			continue
		if to_connect:
			if g == self:
				continue
			if g.in_contact != null and g.in_contact.get_ref() != null:
				continue
			if g._speed > 0:
				continue
		var contact_dist = get_gear_radius() + g.get_gear_radius()
		if to_connect:
			if global_position.distance_to(g.global_position) < contact_dist:
				g.contact_gear(self)
				g.find_all_connections()
		else:
			gears_in_range.append(weakref(g))
	return(gears_in_range)


func update_rot_offset(gear):
	var t_angle = (2*PI/teeth)
	var new_rotation = relative_rot(gear)
	rot_offset = round((rotation - new_rotation) / t_angle) * t_angle
	

func _physics_process(delta):
	if selected:
		if pickup_snap_pos != null:
			var cursor_dist = global_position.distance_to(get_global_mouse_position())
			if cursor_dist > get_gear_radius() * 0.8:
				pickup_snap_pos = null
		else:
			position = get_global_mouse_position()
	
	if _speed != 0:
		rotation += _speed * delta
		$text.rotation = -rotation
		
	if in_contact != null and in_contact.get_ref() != null:
		rotation = relative_rot(in_contact.get_ref()) + rot_offset
			
		$text.rotation = -rotation 
	$text/teeth2.text = str(rad2deg(rotation))


func relative_rot(gear):
	var vec = (gear.global_position - global_position)
	var r = atan2(vec.x, -vec.y)
	var teeth_ratio = gear.teeth / float(teeth)
	var shift = 0
	if (teeth % 2) == 0:
		shift = (2 * PI / teeth) / 2
	return r * (1 + teeth_ratio) - gear.rotation *\
		teeth_ratio + shift



func _on_Area2D_mouse_entered():
	mouse_ower = true


func _on_Area2D_mouse_exited():
	mouse_ower = false


func _on_Area2D_body_entered(body):
	return


func _on_Area2D_body_exited(body):
	return


func get_save_stats():
	var save_stats = {}
	save_stats.parent_path = get_parent().get_path()
	save_stats.name = name
	save_stats.filename = filename
	save_stats.x_pos = global_position.x
	save_stats.y_pos = global_position.y
	save_stats.speed = _speed
	save_stats.locked = locked
	save_stats.teeth = teeth
	save_stats.rotation = rotation
	save_stats.pinned_to = main_node.is_gear_pinned(self)
	return save_stats
	

func load_save_stats(save_stats):
	global_position = Vector2(save_stats.x_pos, save_stats.y_pos)
	_speed = save_stats.speed
	teeth = int(save_stats.teeth)
	if save_stats.has("name"):
		name = save_stats.name
	if save_stats.has("rotation"):
		rotation = save_stats.rotation
	if _speed > 0:
		modulate = Color.pink
	if save_stats.has("pinned_to"):
		to_pin_gear_to = save_stats.pinned_to
	generate()
