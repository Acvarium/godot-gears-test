extends Node2D
var selected = false
var pined_gear = null
var locked = false
var pickup_snap_pos = null

func _physics_process(delta):
	if selected and !Global.game_mode:
		if pickup_snap_pos != null:
			var cursor_dist = global_position.distance_to(get_global_mouse_position())
			if cursor_dist > 50:
				pickup_snap_pos = null
		else:
			position = get_global_mouse_position()


func get_item_info():
	var item_info_dict = {}
	item_info_dict.type = "pin"
	item_info_dict.name = name
	return item_info_dict
	
	
func pickup():
	selected = true
	pickup_snap_pos = global_position

func drop():
	selected = false
	pickup_snap_pos = null

func get_save_stats():
	var save_stats = {}
	save_stats.parent_path = get_parent().get_path()
	save_stats.filename = filename
	save_stats.x_pos = global_position.x
	save_stats.y_pos = global_position.y
	save_stats.locked = locked
	save_stats.name = name
	
	return save_stats
	

func load_save_stats(save_stats):
	global_position = Vector2(save_stats.x_pos, save_stats.y_pos)
	if save_stats.has("name"):
		name = save_stats.name
