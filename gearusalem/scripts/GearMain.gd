extends Node2D
var gear_preload = load("res://objects/gear.tscn")
var pin_preload = load("res://objects/gear_pin.tscn")
var shelf_item_holder_preload = preload("res://objects/GearShelfUIItem.tscn")
var canvas
var W = Color(1,1,1,0.5)
var picked_item = null
const PIN_RADIUS = 20
const PIN_SNAP_DISTANCE = 100
enum item_types {ANY, GEAR, PIN}
var camera_drag_mode = false
var drag_mode_start_pos = Vector2()
var pin_gear_data = {}
var inspector_panel_is_in = true
var selected_item = null
var next_selection = null
var selected_item_info = {}
enum confirm_actions {SAVE}
onready var action_to_confirm = confirm_actions.SAVE 
var shelf_dict: Dictionary = {}


func update_selected_item_info():
	selected_item_info.clear()
	if selected_item != null and selected_item.get_ref() != null:
		if selected_item.get_ref().has_method("get_item_info"):
			selected_item_info = selected_item.get_ref().get_item_info()
	var ss = ""
	for k in selected_item_info.keys():
		ss += k + ": " +  selected_item_info[k] + "\n"
	$CanvasLayer/Mover/Inspector/C/ItemInfo.text = ss
	

func to_pin_gear(gear, pin):
	pin_gear_data[pin.get_path()] = weakref(gear)
	

func to_unpin_gear(gear):
	for k in pin_gear_data.keys():
		if pin_gear_data[k].get_ref() == gear:
			pin_gear_data.erase(k)
			return

func is_gear_pinned(gear):
	for k in pin_gear_data.keys():
		var g = pin_gear_data[k]
		if g.get_ref() == gear:
			return k
	return null
	
	
func _ready():
	randomize()
	toggle_inspector_panel()
	canvas = $canvas


func get_pin_in_range(gear_pos: Vector2):
	var closest_pin = null
	var closest_pin_dist = INF
	for p in get_tree().get_nodes_in_group("gear_pin"):
		if pin_gear_data.keys().has(p.get_path()):
			continue
		var current_pin_dist = gear_pos.distance_to(p.global_position)
		if current_pin_dist < PIN_SNAP_DISTANCE:
			if current_pin_dist < closest_pin_dist:
				closest_pin_dist = current_pin_dist
				closest_pin = p
	return closest_pin


func find_item_under_mouse(mouse_pos: Vector2, item_type: int = item_types.ANY):
	# find pin
	var closest_pin = null
	var closest_pin_dist = INF
	if item_type == item_types.ANY or item_type == item_types.PIN:
		for p in get_tree().get_nodes_in_group("gear_pin"):
			var current_pin_dist = mouse_pos.distance_to(p.global_position)
			if current_pin_dist < PIN_RADIUS:
				if current_pin_dist < closest_pin_dist:
					closest_pin_dist = current_pin_dist
					closest_pin = p
	# find gear
	var closest_gear_dist = INF
	var closest_gear = null
	if item_type == item_types.ANY or item_type == item_types.GEAR:
		for g in get_tree().get_nodes_in_group("gear"):
			if g.is_on_shelf:
				continue
			var current_gear_dist = mouse_pos.distance_to(g.global_position)
			if current_gear_dist < g.get_gear_radius():
				if current_gear_dist < closest_gear_dist:
					closest_gear_dist = current_gear_dist
					closest_gear = g
	if item_type == item_types.PIN:
		return closest_pin
	elif item_type == item_types.GEAR:
		return closest_gear
	elif item_type == item_types.ANY:
		if closest_gear_dist < closest_pin_dist or closest_pin == null:
			return closest_gear
		else:
			return closest_pin


func mouse_action_allowed():
	if $CanvasLayer/ConfirmPopup.visible:
		return false
	var mouse_pos = get_viewport().get_mouse_position()
	if mouse_pos.y > (get_viewport_rect().size.y - 50):
		return false
	var inspector_size = 50
	if inspector_panel_is_in:
		inspector_size = 250
	if mouse_pos.x > (get_viewport_rect().size.x - inspector_size):
		return false
	return true


func clean_scene():
	selected_item = null
	shelf_dict.clear()
	pin_gear_data.clear()
	for g in $gears.get_children():
		g.name = "q"
		g.queue_free()
	for p in $pins.get_children():
		p.name = "q"
		p.queue_free()
	for s in $ShelfCanvas/Shelf/ShelfItems.get_children():
		s.name = "q"
		s.queue_free()


func _process(delta):
	if !Global.game_mode:
		if selected_item != null:
			if selected_item.get_ref() != null:
				$extra/Selector.global_position = selected_item.get_ref().global_position
				$extra/Selector.visible = true
			else:
				$extra/Selector.visible = false
		else:
			$extra/Selector.visible = false

	var ss = ""
	canvas.lines = []
	for gear in $gears.get_children():
		if gear.is_on_shelf:
			continue
		if gear.in_contact and gear.in_contact.get_ref() != null:
			canvas.lines.append([gear.global_position,gear.in_contact.get_ref().global_position, W])
	canvas.update()
	$CanvasLayer/InfoLabel.text = ss
	if !Global.game_mode:
		if next_selection != null and is_instance_valid(next_selection):
			selected_item = weakref(next_selection)
			next_selection = null
	update_selected_item_info()


func rebuild_gear_struct():
	var moving_gear = null
	for g in get_tree().get_nodes_in_group("gear"):
		if g._speed > 0:
			moving_gear = g
		g.in_contact = null
	if moving_gear:
		moving_gear.find_all_connections()
	for i in range(5):
		var sead_gear = null
		var sead_gears = []
		for g in get_tree().get_nodes_in_group("gear"):
			if g.is_on_shelf:
				continue
			if g._speed == 0 and g.in_contact == null and ! sead_gears.has(g):
				sead_gear = g
				sead_gears.append(g)
				break
		if sead_gear != null:
			sead_gear.find_all_connections()


func zoom_camera(value):
	var current_zoom = $GearViewCamera.zoom.x
	var new_zoom = clamp(current_zoom + value, 1.3, 2.3)
	$GearViewCamera.zoom = Vector2(new_zoom, new_zoom)


func add_item(item_type, pos = null):
	var item_pos = $GearViewCamera.global_position
	var screen_size = get_viewport_rect().size
	item_pos += Vector2(0, -screen_size.y / 2 + 70) * $GearViewCamera.zoom.x
	if pos != null:
		item_pos = pos
	if item_type == item_types.GEAR:
		var gear = gear_preload.instance()
		$gears.add_child(gear)
		gear.global_position = item_pos
		next_selection = gear
		gear.name = "gear" + str(randi())
		return gear
	elif item_type == item_types.PIN:
		var pin = pin_preload.instance()
		$pins.add_child(pin)
		pin.global_position = item_pos
		pin.name = "pin" + str(randi())
		next_selection = pin
		return pin
	
func _input(event):
	if event is InputEventScreenTouch and event.index == 0:
		if event.is_pressed() and mouse_action_allowed():
			var item_under_mouse = find_item_under_mouse(get_global_mouse_position())
			if item_under_mouse != null:
				
				next_selection = item_under_mouse
#				if Global.game_mode or (selected_item != null and next_selection == selected_item.get_ref()):
				picked_item = weakref(item_under_mouse)
				if picked_item.get_ref().is_in_group("gear"):
					to_unpin_gear(picked_item.get_ref())
				item_under_mouse.pickup()
			else:
				next_selection = null
				camera_drag_mode = true
		else:
			if picked_item != null:
				if picked_item.get_ref() != null:
					if picked_item.get_ref().is_in_group("gear"):
						var pin = get_pin_in_range(picked_item.get_ref().global_position)
						if pin != null:
							picked_item.get_ref().global_position = pin.global_position
							to_pin_gear(picked_item.get_ref(), pin)
					picked_item.get_ref().drop()
			picked_item = null
			camera_drag_mode = false
	elif camera_drag_mode:
		if event is InputEventScreenDrag and event.index == 0:
			$GearViewCamera.global_position -= event.relative * $GearViewCamera.zoom.x
			
	if event.is_action_pressed("w_up"):
		if camera_drag_mode:
			zoom_camera(-0.1)
	elif event.is_action_pressed("w_down"):
		if camera_drag_mode:
			zoom_camera(0.1)

func toggle_inspector_panel():
	if $CanvasLayer/Mover/Inspector/AnimationPlayer.is_playing():
		return
	inspector_panel_is_in = !inspector_panel_is_in
	if inspector_panel_is_in:
		$CanvasLayer/Mover/Inspector/AnimationPlayer.play("move_in")
	else:
		$CanvasLayer/Mover/Inspector/AnimationPlayer.play("move_out")

func _on_InspectorMoveButton_pressed():
	toggle_inspector_panel()


func _on_AddGearButton_pressed():
	add_item(item_types.GEAR)


func _on_AddPinButton_pressed():
	add_item(item_types.PIN)


func _on_DeleteSelectedButton_pressed():
	if selected_item != null and selected_item.get_ref() != null:
		selected_item.get_ref().queue_free()
		selected_item = null

func get_level_name():
	return $CanvasLayer/Mover/Inspector/C/C/LevelFileNameInput.text

func _on_SaveButton_pressed():
	var level_name = get_level_name()
	if level_name in Global.list_levels():
		confirm_popup(confirm_actions.SAVE)
	else:
		Global.save_level(level_name)


func confirm_popup(action):
	action_to_confirm = action
	$CanvasLayer/ConfirmPopup.popup()

func _on_LoadButton_pressed():
	Global.load_saved(get_level_name())


func rebuild_timer_start():
	$extra/RebuildTimer.start()

func _on_RebuildTimer_timeout():
	rebuild_gear_struct()

func build_levels_list():
	var levels = Global.list_levels()
	var popup = $CanvasLayer/Mover/Inspector/C/LevelsListPopUp
	popup.clear()
	if levels.size() > 0:
		for i in range(levels.size()):
			popup.add_item(levels[i])
	popup.popup()


func _on_LevelsListPopUp_index_pressed(index):
	var ln_input = $CanvasLayer/Mover/Inspector/C/C/LevelFileNameInput
	var ln_popup = $CanvasLayer/Mover/Inspector/C/LevelsListPopUp
	ln_input.text = ln_popup.get_item_text(index)
#	$CanvasLayer/Mover/Inspector/C/LevelsListPopUp.visible = false


func _on_LevelListButton_pressed():
	build_levels_list()


func add_gear_to_shelf(gear):
	if gear.is_on_shelf:
		return
	to_unpin_gear(gear)
	gear.contact_gear(null)
	gear.put_on_shelf()
	var shelf_item_holder = shelf_item_holder_preload.instance()
	$ShelfCanvas/Shelf/ShelfItems.add_child(shelf_item_holder)
	shelf_item_holder.set_gear(gear)
	rebuild_gear_struct()
	selected_item = null


func add_selected_gear_to_shelf():
	if selected_item != null and selected_item.get_ref() != null:
		if selected_item.get_ref().is_in_group("gear"):
			 add_gear_to_shelf(selected_item.get_ref())
	
func remove_gear_from_shelf(gear, gear_holder_button):
	if gear == null:
		return
	if !gear.is_in_group("gear"):
		return
	if !gear.is_on_shelf:
		return
	gear.global_position = get_global_mouse_position()
	gear.remove_from_shelf()
	gear_holder_button.queue_free()
	next_selection = gear
	picked_item = weakref(gear)
	to_unpin_gear(gear)
	gear.pickup(true)
	camera_drag_mode = false


func create_pin_for_gear(gear):
	if is_gear_pinned(gear):
		return
	var pos = gear.global_position
	var _pin = add_item(item_types.PIN, pos)
	to_pin_gear(gear, _pin)

func pin_loaded_gears():
	for g in get_tree().get_nodes_in_group("gear"):
		if g.to_pin_gear_to != null and has_node(g.to_pin_gear_to):
			to_pin_gear(g, get_node(g.to_pin_gear_to))
	

func _on_YesButton_pressed():
	if action_to_confirm == confirm_actions.SAVE:
		Global.save_level(get_level_name())
	$CanvasLayer/ConfirmPopup.visible = false


func _on_NoButton_pressed():
	$CanvasLayer/ConfirmPopup.visible = false


func _on_AddToShelfButton_pressed():
	add_selected_gear_to_shelf()


func _on_CreatePinForSelectedButton_pressed():
	if selected_item != null and selected_item.get_ref() != null:
		if selected_item.get_ref().is_in_group("gear"):
			create_pin_for_gear(selected_item.get_ref())


func _on_GameModeCheck_toggled(button_pressed):
	Global.game_mode = $CanvasLayer/Mover/Inspector/C/Control/GameModeCheck.pressed
	$extra/Selector.visible = false
	rebuild_gear_struct()
