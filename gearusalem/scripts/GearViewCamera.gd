extends Camera2D

func get_save_stats():
	var save_stats = {}
	save_stats.node_path = get_path()
	save_stats.x_pos = global_position.x
	save_stats.y_pos = global_position.y
	save_stats.zoom = zoom.x
	return save_stats

func load_save_stats(save_stats):
	global_position = Vector2(save_stats.x_pos, save_stats.y_pos)
	zoom = Vector2(save_stats.zoom, save_stats.zoom)



